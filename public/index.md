<img src="elixir-norway.png" width="150">  <img src="nmbu.png" width="200"> 

## Workshop on using High-Performance Cluster and ELIXIR tools for Bioinformatics 

__Date: 30th October 2019__

__Venue: [Room U122](http://bit.ly/2lN9dAT), Urbygningen, NMBU Ås__

#### [Program & registration](program)

\


#### Morning Session (10:30-12:00):
*Introduction to UNIX & High-Performance Cluster (HPC). For Windows users: if you haven't do that, please install an SSH client such as [MobaXterm](https://mobaxterm.mobatek.net/download-home-edition.html) (download the "Portable edition") or [PuTTY](http://www.putty.org/).*

- [Introduction to Orion](https://orion.nmbu.no/Presentations/2019-Oct/2019-Orion-introduction-handson.pdf)

\


#### Lunch (12:00-12:45): 
*A coffee bar and a canteen is available close to the venue, or you can bring your “matpakke”.*

\ 


#### Afternoon Session (12:45-15:45):
*Introduction to NeLS and Galaxy. For those who hasn't used NeLS before, please log in to [NeLS](https://nels.bioinfo.no/) using your FEIDE id so that your NeLS profile would be automatically created.*

- [Introduction to NeLS and Galaxy - hands-on practice](https://prezi.com/p/milsk3zle5xo/elixir-norway-nels-and-galaxy-introduction/)

- Break (14:15:14:30)

- [An example of ATAC-seq workflow using galaxy](https://drive.google.com/file/d/1f6lV3VyC-s9UUS42v7texVc5gxqm3Gf0/view?usp=sharing)

\


#### Some useful links

- Introduction to Orion: https://gitlab.com/cigene/computational/orion-support/wikis/home

- Norwegian e-Infrastructure for Life Sciences (NeLS): http://nels.bioinfo.no/

- Some tutorials for data management in NeLS/Galaxy: https://bioinfo.no/temp/tutorials 

- A Galaxy tutorial: https://galaxyproject.github.io/training-material/topics/introduction/tutorials/galaxy-intro-short/tutorial.html

- Five Galaxy instances of ELIXIR Norway (each instance could have different tools / shared workflows): 
	+ https://galaxy-nmbu.bioinfo.no/
	+ https://galaxy-ntnu.bioinfo.no/
	+ https://galaxy-uib.bioinfo.no/
	+ https://galaxy-uit.bioinfo.no/
	+ https://galaxy-uio.bioinfo.no/


#### Surveys
- In order to organize a better workshop in the future, we need your feedback. Please help us by filling this [survey.](https://form.nmbu.no/view.php?id=588836)

- ELIXIR Norway is now performing a user [survey](https://www.elixir-norway.org/news/17/63/Please-help-us-improve-our-bioinformatics-services) to evaluate and identify gaps in our bioinformatics services. We are grateful if you would take the time to fill out the survey, and earn a chance to win an online gift card of the value of 1000 NOK from GoGift.

Contact ELIXIR help desk: contact@bioinfo.no
